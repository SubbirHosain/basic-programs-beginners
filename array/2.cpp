/**
 * C program to find sum of all elements of array
 */

#include <stdio.h>

int main()
{
    int arr[100];
    int i, n, sum=0;

    /*
     * Reads size of the array and elements in array from user
     */
    printf("Enter size of the array: ");
    scanf("%d", &n);
    printf("Enter %d elements in the array: ", n);
    for(i=0; i<n; i++)
    {
        scanf("%d", &arr[i]);
    }

    /*
     * Adds each element of array to sum
     */
    for(i=0; i<n; i++)
    {
        sum += arr[i];
    }

    printf("Sum of all elements of array = %d", sum);

    return 0;
}
