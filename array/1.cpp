/**
 * C program to read and print elements in an array
 */

#include <stdio.h>

int main()
{
    int arr[10]; //Declares an array of size 10
    int i;

    /*
     * Reads elements in array
     */
    printf("Enter 10 elements in the array : ");
    for(i=0; i<10; i++)
    {
        scanf("%d", &arr[i]);
    }

    /*
     * Prints all elements of array
     */
    printf("\nElements in array are: ");
    for(i=0; i<10; i++)
    {
        printf("%d\t", arr[i]);
    }

    return 0;
}
