/**
 * C program to print all negative elements in array
 */

#include <stdio.h>

int main()
{
    int arr[100]; //Declares an array of size 100
    int i, n;

    /*
     * Reads size and elements of array
     */
    printf("Enter size of the array : ");
    scanf("%d", &n);

    printf("Enter elements in array : ");
    for(i=0; i<n; i++)
    {
        scanf("%d", &arr[i]);
    }

    /*
     * Finds all negative elements in array
     */
    printf("\nAll negative elements in array are : ");
    for(i=0; i<n; i++)
    {
        if(arr[i]<0)
        {
            printf("%d\t", arr[i]);
        }
    }

    return 0;
}
