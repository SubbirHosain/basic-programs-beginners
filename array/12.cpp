/**
 * C program to merge two sorted array in ascending order
 */

#include <stdio.h>

int main()
{
    int arr1[100], arr2[100], arr3[200];
    int size1, size2, size3;
    int i, j, k;

    /*
     * Read size of first array and elements in first array
     */
    printf("Enter the size of first array : ");
    scanf("%d", &size1);
    printf("Enter elements in first array : ");
    for(i=0; i<size1; i++)
    {
        scanf("%d", &arr1[i]);
    }

    /*
     * Reads size of second array and elements in second array
     */
    printf("\nEnter the size of second array : ");
    scanf("%d", &size2);
    printf("Enter elements in second array : ");
    for(i=0; i<size2; i++)
    {
        scanf("%d", &arr2[i]);
    }

    /* size of merged array is size_of_first_array + size_of_second_array */
    size3 = size1 + size2;

    /*
     * Merge two array in ascending order
     */
    for(i=0, j=0, k=0; i<size3; i++)
    {
        if(arr1[j] < arr2[k])
        {
            arr3[i] = arr1[j];
            j++;
        }
        else
        {
            arr3[i] = arr2[k];
            k++;
        }
    }

    /*
     * Prints the merged array
     */
    printf("\nArray merged in ascending order : ");
    for(i=0; i<size3; i++)
    {
        printf("%d\t", arr3[i]);
    }

    return 0;
}
