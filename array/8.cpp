/**
 * C program to delete all duplicate elements from array
 */

#include <stdio.h>

int main()
{
    int arr[100]; //Declares an array of size 100
    int size; //Total number of elements in array
    int i, j, k; //Used for loop

    /*
     * Reads size and elements of array
     */
    printf("Enter size of the array : ");
    scanf("%d", &size);

    printf("Enter elements in array : ");
    for(i=0; i<size; i++)
    {
        scanf("%d", &arr[i]);
    }

    /*
     * Finds all duplicate elements in array
     */
    for(i=0; i<size; i++)
    {
        for(j=i+1; j<size; j++)
        {
            /* If any duplicate found */
            if(arr[i]==arr[j])
            {
                for(k=j; k<size-1; k++)
                {
                    arr[k] = arr[k+1];
                }

                /* Decrement size after removing one duplicate element */
                size--;
            }
        }
    }

    /*
     * Print array after deleting duplicate elements
     */
    printf("\nArray elements after deleting duplicates : ");
    for(i=0; i<size; i++)
    {
        printf("%d\t", arr[i]);
    }

    return 0;
}

