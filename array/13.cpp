/**
 * C program to find reverse of an array
 */

 #include <stdio.h>

 int main()
 {
     int arr[100], reverse[100];
     int size, i, j;

     /*
      * Reads the size of array and elements in array
      */
     printf("Enter size of the array: ");
     scanf("%d", &size);
     printf("Enter elements in array: ");
     for(i=0; i<size; i++)
     {
         scanf("%d", &arr[i]);
     }

     /*
      * Reverse the array
      */
     j=0;
     for(i=size-1; i>=0; i--)
     {
         reverse[j] = arr[i];
         j++;
     }

     /*
      * Prints the reversed array
      */
     printf("\nReversed array : ");
     for(i=0; i<size; i++)
     {
         printf("%d\t", reverse[i]);
     }

     return 0;
 }
