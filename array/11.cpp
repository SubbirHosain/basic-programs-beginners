/**
 * C program to count total number of negative elements in array
 */

#include <stdio.h>

int main()
{
    int arr[100]; //Declares an array of size 100
    int i, n, count=0;

    /*
     * Reads size and elements of array
     */
    printf("Enter size of the array : ");
    scanf("%d", &n);

    printf("Enter elements in array : ");
    for(i=0; i<n; i++)
    {
        scanf("%d", &arr[i]);
    }

    /*
     * Counts total number of negative elements in array
     */
    for(i=0; i<n; i++)
    {
        if(arr[i]<0)
        {
            count++;
        }
    }

    printf("\nTotal number of negative elements = %d", count);

    return 0;
}
