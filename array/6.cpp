/*
 * C program to delete an element from array at specified position
 */

#include <stdio.h>

int main()
{
    int arr[100];
    int i, n, position;

    /*
     * Reads size and elements in array from user
     */
    printf("Enter size of the array : ");
    scanf("%d", &n);

    printf("Enter elements in array : ");
    for(i=0; i<n; i++)
    {
        scanf("%d", &arr[i]);
    }

    /*
     * Reads the position to be deleted from user
     */
    printf("Enter the element position to delete : ");
    scanf("%d", &position);

    /*
     * Check if the position is valid
     */
    if(position==n+1 || position<0)
    {
        printf("Invalid position! Please enter position between 1 to %d", n);
    }
    else
    {
        /*
         * If delete position is valid then delete the specified element
         */
        for(i=position-1; i<n-1; i++)
        {
            arr[i] = arr[i+1];
        }
    }

    /*
     * Prints the array after delete operation
     */
    printf("\nElements of array after delete are : ");
    for(i=0; i<n-1; i++)
    {
        printf("%d\t", arr[i]);
    }

    return 0;
}
