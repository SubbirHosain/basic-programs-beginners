
#include <stdio.h>

int main()
{
    int num, sum=0;

    /*
     * Reads a number from user
     */
    printf("Enter any number to find sum of its digit: ");
    scanf("%d", &num);

    /*
     * Finds the sum of digits
     */
    while(num!=0)
    {
        /* Find the last digit from num and add to sum */
        sum += num % 10;

        /* Removes last digit from num */
        num = num / 10;
    }

    printf("\nSum of digits = %d", sum);

    return 0;
}

