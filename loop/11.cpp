
#include <stdio.h>

int main()
{
    int n;
    long product=1;

    /*
     * Reads a number from user
     */
    printf("Enter any number to calculate product of digit: ");
    scanf("%d", &n);

    /*
     * Calculates product of all digits
     */
    for( ; n!=0 ; )
    {
        product = product * (n % 10); //Gets the last digit from n and multiplies to product.
        n = n / 10; //Removes the last digit from n.
    }

    printf("\nProduct of digits = %ld", product);

    return 0;
}
