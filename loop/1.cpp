/**
 * C program to print all natural numbers from 1 to n
 */

#include <stdio.h>

int main()
{
    int i, n;

    /*
     * Reads the value of n from user
     */
    printf("Enter any number: ");
    scanf("%d", &n);

    printf("Natural numbers from 1 to %d : \n", n);

    /*
     * Starts loop counter from 1 (i=1) and goes till n (i<=n)
     * And in each repetition prints the value of i
     */
    for(i=1 ; i<=n; i++)
    {
        printf("%d\n", i);
    }

    return 0;
}


