#include <stdio.h>

int main()
{
    int i, n;

    /*
     * Reads a number from user
     */
    printf("Enter value of n: ");
    scanf("%d", &n);

    /*
     * Runs a loop from n and goes till 1
     * Decrementing the value
     */
    for(i=n; i>=1; i--)
    {
        printf("%d\n", i);
    }

    return 0;
}

