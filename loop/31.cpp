
#include <stdio.h>

int main()
{
    int i, j, n, sum = 0;

    /*
     * Reads a number from user
     */
    printf("Enter any number to print perfect number up to: ");
    scanf("%d", &n);

    printf("\nAll Perfect numbers between 1 to %d:\n", n);


    /*
     * Finds all Perfect number within given range
     */
    for(i=1; i<=n; i++)
    {
        sum = 0;

        /*
         * Checks whether the current number i is Perfect number or not
         */
        for(j=1; j<i; j++)
        {
            if(i%j==0)
            {
                sum += j;
            }
        }

        /*
         * If the current number i is Perfect number
         */
        if(sum==i)
        {
            printf("%d is Perfect Number\n", i);
        }
    }

    return 0;
}
