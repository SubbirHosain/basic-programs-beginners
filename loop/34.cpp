
#include <stdio.h>

int main()
{
    int i, j, n, isPrime, sum=0;

    /*
     * Reads a number from user
     */
    printf("Find sum of all prime between 1 to : ");
    scanf("%d", &n);

    /*
     * Finds all prime numbers between 1 to n
     */
    for(i=2; i<=n; i++)
    {

        /*
         * Checks if the current number i is Prime or not
         */
        isPrime = 1;
        for(j=2; j<=i/2 ;j++)
        {
            if(i%j==0)
            {
                isPrime = 0;
                break;
            }
        }

        /*
         * If i is Prime then add to sum
         */
        if(isPrime==1)
        {
            sum += i;
        }
    }

    printf("Sum of all prime numbers between 1 to %d = %d", n, sum);

    return 0;
}

