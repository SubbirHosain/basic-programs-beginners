#include <stdio.h>

int main()
{
    int i;

    printf("All even numbers from 1 to 100 are: \n");

    /*
     * Starts loop from 1 and increments by 1
     */
    for( i=1 ; i<=100 ; i++)
    {
        /* Print the number if it is even */
        if(i%2==0)
        {
            printf("%d\n", i);
        }
    }

    return 0;
}

