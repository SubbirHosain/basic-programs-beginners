
#include <stdio.h>

int main()
{
    int i, n, sum=0;

    /*
     * Reads a number from user
     */
    printf("Enter any number to find sum of first and last digit: ");
    scanf("%d", &n);

    /* Checks whether the number is greater than 2 digits */
    if(n>10)
    {
        /* Adds last digit to sum */
        sum += n % 10;
    }

    /*
     * Finds the first digit by dividing n by 10 until first digit is left
     */
    while(n>10)
    {
        n = n / 10;
    }

    /* Adds first digit to sum */
    sum += n;

    printf("\nSum of first and last digit = %d", sum);

    return 0;
}
