
#include <stdio.h>

int main()
{
    int a, b, c, n;

    /*
     * Reads a number from user
     */
    printf("Enter value of n to print Fibonacci series : ");
    scanf("%d", &n);


    a = 0;
    b = 1;
    c = 0;

    while(c<=n)
    {
        printf("%d, ", c);

        a=b;
        b=c;
        c=a+b;
    }

    return 0;
}
