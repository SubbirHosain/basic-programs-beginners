

#include <stdio.h>

int main()
{
    int i, n, sum=0;

    /*
     * Reads a number from user
     */
    printf("Enter any number: ");
    scanf("%d", &n);

    /*
     * Finds the sum of all odd number
     */
    for( i=1 ; i<=n ; i+=2 )
    {
        sum += i;
    }

    printf("\nSum of all odd number between 1 to %d = %d", n, sum);

    return 0;
}

