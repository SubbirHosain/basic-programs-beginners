
#include <stdio.h>

int main()
{
    int i, num;
    long fact=1;

    /*
     * Reads a number from user
     */
    printf("Enter any number to calculate factorial: ");
    scanf("%d", &num);

    /*
     * Find the factorial of num
     */
    for(i=1; i<=num; i++)
    {
        fact = fact * i;
    }

    printf("Factorial of %d = %ld", num, fact);

    return 0;
}
