
#include <stdio.h>

int main()
{
    int i, n, num, sum=0;
    long fact;

    /*
     * Reads a number from user
     */
    printf("Enter any number to check Strong number: ");
    scanf("%d", &n);

    num = n;

    /*
     * Calculates sum of factorial of digits
     */
    while(n!=0)
    {

        /*
         * Finds the factorial
         */
        fact = 1;
        for(i=1; i<=n%10; i++)
        {
            fact = fact * i;
        }

        /* Adds the factorial to sum */
        sum = sum + fact;

        n = n/10;
    }

    /*
     * If sum of factorial of digits is equal to original number then it is Strong number
     */
    if(sum==num)
    {
        printf("\n%d is Strong number", num);
    }
    else
    {
        printf("\n%d is not Strong number", num);
    }

    return 0;
}
