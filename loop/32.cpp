
#include <stdio.h>

int main()
{
    int i, j, cur, n;
    long fact, sum;

    /*
     * Reads a number from user
     */
    printf("Find Strong numbers between 1 to ");
    scanf("%d", &n);

    printf("All Strong numbers between 1 to %d are:\n", n);

    /*
     * Finds all Strong numbers between 1 to n
     */
    for(i=1; i<=n; i++)
    {
        /* Number to check whether it is Strong number or not */
        cur = i;

        sum = 0;

        /*
         * Finds the sum of factorial of each digits
         */
        while(cur!=0)
        {
            fact = 1;

            /* Computes factorial of last digit i.e. cur%10 */
            for( j=1; j<=cur%10; j++)
            {
                fact = fact * j;
            }

            sum = sum + fact;

            cur = cur / 10;
        }

        /*
         * Checks if it is Strong number then print it
         */
        if(sum==i)
        {
            printf("%d is Strong number\n", i);
        }
    }

    return 0;
}

