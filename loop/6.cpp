#include <stdio.h>

int main()
{
    int i, n, sum=0;

    /*
     * Reads a number from user
     */
    printf("Enter any number: ");
    scanf("%d", &n);

    for(i=2; i<=n; i+=2)
    {
        /* Calculates sum of even number */
        sum += i;
    }

    printf("\nSum of all even number between 1 to %d = %d", n, sum);

    return 0;
}
