

 #include <stdio.h>

 int main()
 {
     long base, exponent;
     long long power = 1;
     long i;

     /*
      * Reads base and exponent from user
      */
     printf("Enter base: ");
     scanf("%ld", &base);
     printf("Enter exponent: ");
     scanf("%ld", &exponent);

     /*
      * Finds the power
      */
     for(i=1; i<=exponent; i++)
     {
         power = power * base;
     }

     printf("\n%ld ^ %ld = %lld", base, exponent, power);

     return 0;
 }

