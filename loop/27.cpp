
#include <stdio.h>

int main()
{
    int i, num, sum = 0;

    /*
     * Reads a number from user
     */
    printf("Enter any number to check perfect number: ");
    scanf("%d", &num);

    /*
     * Finds the sum of all proper divisors
     */
    for(i=1; i<num; i++)
    {
        /* If i is a divisor of num */
        if(num%i==0)
        {
            sum += i;
        }
    }

    /*
     * Checks whether the sum of proper divisors is equal to num
     */
    if(sum==num)
    {
        printf("\n%d is a Perfect number", num);
    }
    else
    {
        printf("\n%d is not a Perfect number", num);
    }

    return 0;
}
