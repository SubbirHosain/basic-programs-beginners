#include <stdio.h>

int main()
{
    int i;

    printf("All odd numbers from 1 to 100 are: \n");

    /*
     * Starts a loop from 1 and increments its by 1
     */
    for(i=1; i<=100; i++)
    {
        /* Check if the number is odd then print */
        if(i%2!=0)
        {
            printf("%d\n",i);
        }
    }

    return 0;
}
