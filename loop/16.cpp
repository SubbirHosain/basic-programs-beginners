
#include <stdio.h>

int main()
{
    int n, num, rev = 0;

    /*
     * Reads a number from user
     */
    printf("Enter any number to check palindrome: ");
    scanf("%d", &n);

    num = n; //Copies original value to num.

    /*
     * Finds the reverse of n and stores in rev
     */
    for( ; n!=0 ; )
    {
        rev = (rev * 10) + (n % 10);
        n = n/10;
    }

    /*
     * Check if reverse of n is equal to original num or not
     */
    if(rev==num)
    {
        printf("\nNumber is palindrome.");
    }
    else
    {
        printf("\nNumber is not palindrome.");
    }

    return 0;
}
