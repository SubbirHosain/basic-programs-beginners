
#include <stdio.h>

int main()
{
    int i, num1, num2, max, lcm=1;

    /*
     * Reads two numbers from user
     */
    printf("Enter any two numbers to find LCM: ");
    scanf("%d %d", &num1, &num2);

    max = (num1>num2) ? num1 : num2;

    /*
     * Finds LCM till the Max value of Integer
     */
    for(i=max;  ; i+=max)
    {
        /*
         * If i is a multiple of both numbers
         */
        if(i%num1==0 && i%num2==0)
        {
            lcm = i;
            break;
        }
    }

    printf("\nLCM of %d and %d = %d\n", num1, num2, lcm);

    return 0;
}
