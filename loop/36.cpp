

#include <stdio.h>

long fact(int n);

int main()
{
    int n, k, num, i;
    long term;

    printf("Enter number of rows : ");
    scanf("%d", &num);

    for(n=0; n<=num; n++)
    {
        //Prints spaces
        for(i=n; i<=num; i++)
            printf("%*c", 2,' ',' ');

        for(k=0; k<=n; k++)
        {
            term = fact(n)/( fact(k) * fact(n-k));
            printf("%4ld", term);
        }

        printf("\n");
    }

    return 0;
}
